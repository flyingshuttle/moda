package com.balab.edu.m2

/**
  * Created by dev-env on 19/5/16.
  */
object MainApp {

  def main(args:Array[String]) = {
      val conversion  = new Conversion()

    Util.moreThan10Words()
  }

}


object Util {

  def getLargestElement() = {
    val a = Array(20, 12, 6, 15, 2, 9)
    val c = a.reduceLeft(_ max _)

    print(c)
  }

  def reduceFac(value: Int) = {
    if(value > 0) {
      (1 to value).reduceLeft(_ * _)
    }
    else {
      -1 * (1 to math.abs(value)).reduceLeft(_ * _)
    }
  }


def revereLineInFile() = {
  val stream = getClass.getResourceAsStream("/16052016.txt")
  val lines = scala.io.Source.fromInputStream( stream ).getLines.toArray
  val revLines = lines.reverse

  print(revLines)
}

  def moreThan10Words()={

    val stream = getClass.getResourceAsStream("/16052016.txt")
    val strLines = scala.io.Source.fromInputStream( stream ).mkString

    val longWords = for(a <- strLines.split(' ') if a.length >= 10) yield a

    print(longWords)

  }

}