package com.balab.edu.m2

/**
  * Created by dev-env on 19/5/16.
  */
class AccountInfo {
  private var _balance = 0

  def balance = _balance

  def deposit(money: Int) = _balance += money
  def withdraw(money: Int) = if(money < _balance) _balance -= money

}