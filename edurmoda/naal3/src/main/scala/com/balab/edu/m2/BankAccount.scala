package com.balab.edu.m2

/**
  * Created by dev-env on 19/5/16.
  */
class BankAccount (initBal: Double) {

  private var balance = initBal

  def deposit(amount: Double) = {
    balance += amount
    balance
  }

  def withdraw(amount: Double) = {
    balance -= amount
    balance
  }

  def getBalance:Double = balance

}


class CheckingAccount(initBal: Double) extends BankAccount(initBal) {

  override def deposit(amount: Double) = {
    super.deposit( amount )
    deductFees()
  }

  override  def withdraw(amount: Double) = {
    super.withdraw( amount )
    deductFees()
  }

  private def deductFees() = {
      val fees:Double = BankAccount.TRANSACTION_FEE
      super.withdraw( fees )
  }

}

object BankAccount {
  val FREE_TRANSACTIONS = 3
  val TRANSACTION_FEE = 1
}


object AccountTest {


  def main(arg:Array[String]) = {

    val checkingAccount  = new CheckingAccount(0)
    checkingAccount.deposit(1000)
    checkingAccount.withdraw(500)
    checkingAccount.deposit(1000)
    checkingAccount.withdraw(500)
    checkingAccount.deposit(1000)

  }

}
