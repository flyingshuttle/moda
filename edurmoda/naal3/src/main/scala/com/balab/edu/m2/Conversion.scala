package com.balab.edu.m2

/**
  * Created by dev-env on 19/5/16.
  */
class Conversion {

  println(Conversion.inchesToFeet("400"))
  println(Conversion.milesToKm("400"))
  println(Conversion.poundsToKilos("400"))

}


object Conversion {


  def inchesToFeet(input:String): String = {

    val inches:Int = input.toInt
    val feet = inches / 12
    val leftover = inches % 12

    s""+feet + " Feet and " + leftover + " inches"

  }


  def milesToKm(input:String):String = {

    val miles:Double = input.toDouble
    val kilometers = miles * 1.609344

    s""+ miles + "miles is equal to " + kilometers + "kilometers "

  }

  def poundsToKilos(input:String):String= {

    val pounds:Double = input.toDouble
    val kilograms:Double = pounds * 0.454

    s""+ pounds + " pounds is " + kilograms + " killograms"
  }

}


